import '../styles/app.sass';

document.addEventListener('DOMContentLoaded', () => {
    $('.ui.dropdown').dropdown();

    const tags = $('.js-tags');

    tags.dropdown({
        allowAdditions: true,
    });

    $('.js-tags input').on('keydown', (e) => {
        if (e.keyCode === 13) {
            e.preventDefault();

            let event = jQuery.Event("keydown");
            event.which = 13;

            $(e.target).trigger(event);

            return false;
        }
    });
});
