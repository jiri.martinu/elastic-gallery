<?php

namespace App\Controller;

use App\Entity\Filter\PhotoSearchFilter;
use App\Form\PhotoSearchType;
use App\Repository\PhotoRepository;
use App\Utils\ElasticQueries;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage", methods={"GET","POST"})
     */
    public function index(Request $request, PhotoRepository $photoRepository, ContainerInterface $container): Response
    {
        $photoSearchFilter = new PhotoSearchFilter();

        $form = $this->createForm(PhotoSearchType::class, $photoSearchFilter)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var PhotoSearchFilter $data */
            $data = $form->getData();

            return $this->render('homepage/index.html.twig', [
                'photoSearchForm' => $form->createView(),
		// This should be in Manager!
                'photos' => $data->source == PhotoSearchFilter::DOCTRINE ?
                    $photoRepository->findByFilter($data) :
                    $container->get('fos_elastica.finder.app.photo')->find(ElasticQueries::createPhotoQuery($data), 10),
            ]);
        }

        return $this->render('homepage/index.html.twig', [
            'photoSearchForm' => $form->createView(),
            'photos' => [],
        ]);
    }
}
