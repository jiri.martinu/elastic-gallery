<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use App\Entity\Photo;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

class PhotoFixtures extends Fixture
{
    private $words;

    public function __construct()
    {
        $this->words = file(__DIR__ . '/../Resources/imports/words.txt', FILE_IGNORE_NEW_LINES);
    }

    public function getRandomWord(): string
    {
        $word = '';

        for ($i = 0; $i < rand(1, 10); $i++) {
            $word .= ($i !== 0 ? ' ' : '') . $this->words[rand(0, count($this->words) - 1)];
        }

        return $word;
    }

    public function getRandomTags(): array
    {
        $tags = [];

        for ($i = 0; $i < rand(1, 10); $i++) {
            $tags[] = $this->words[rand(0, count($this->words) - 1)];
        }

        return $tags;
    }

    public function getRandomPath(): string
    {
        return str_replace(' ', '-', $this->getRandomWord()) . '.png';
    }

    public function load(ObjectManager $manager)
    {
        /** @var EntityManager $manager */
        $manager->getConnection()->getConfiguration()->setSQLLogger(null);

        for ($i = 0; $i < 100000; $i++) {
            $photo = new Photo();
            $photo->setTitle($this->getRandomWord());
            $photo->setPath($this->getRandomPath());
            $photo->setTags($this->getRandomTags());

            $manager->persist($photo);

            if ($i % 10000 === 0) {
                dump($i);

                $manager->flush();
            }
        }

        $manager->flush();
    }
}
