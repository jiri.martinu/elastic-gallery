<?php

namespace App\Entity\Filter;

class PhotoSearchFilter
{
    public const DOCTRINE = 'doctrine';
    public const ELASTIC = 'elastic';

    public const SOURCE = [
        self::DOCTRINE,
        self::ELASTIC
    ];

    /** @var null|string */
    public $value;

    /** @var null|array */
    public $tags;

    /** @var null|bool */
    public $source;
}