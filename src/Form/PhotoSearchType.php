<?php

namespace App\Form;

use App\Entity\Filter\PhotoSearchFilter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhotoSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('value', SearchType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Find',
                ],
            ])
            ->add('tags', ChoiceType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Tags',
                    'class' => 'ui fluid multiple search selection dropdown js-tags'
                ],
                'multiple' => true
            ])
            ->add('source', ChoiceType::class, [
                'label' => 'Source',
                'choices' => [
                    'Doctrine' => PhotoSearchFilter::DOCTRINE,
                    'Elastic' => PhotoSearchFilter::ELASTIC
                ],
                'attr' => [
                    'class' => 'ui selection dropdown'
                ]
            ])
        ;

        $builder->get('tags')->resetViewTransformers();
        $builder->get('tags')->addViewTransformer(new CallbackTransformer(
            function ($data) {
                if (empty($data)) {
                    return null;
                }

                return $data;
            },
            function ($data) {
                return $data;
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PhotoSearchFilter::class,
            'attr' => [
                'class' => 'ui form',
                'novalidate' => 'novalidate'
            ],
        ]);
    }
}
