<?php

namespace App\Repository;

use App\Entity\Filter\PhotoSearchFilter;
use App\Entity\Photo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Photo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Photo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Photo[]    findAll()
 * @method Photo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhotoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Photo::class);
    }

    public function findByFilter(PhotoSearchFilter $filter): array
    {
        $qb = $this->createQueryBuilder('p');

        if ($filter->value !== null) {
            $qb->andWhere('p.title LIKE :search')
                ->setParameter('search', '%'. $filter->value . '%');
        }

        if (count($filter->tags) > 0) {
            $tagQueries = [];

            foreach ($filter->tags as $key => $tag) {
                $tagQueries[] = $qb->expr()->like('p.tags', '?'.$key);
                $qb->setParameter($key, '%' . $tag . '%');
            }

            $qb->andWhere(
                $qb->expr()->orX(
                    ...$tagQueries
                )
            );
        }

        $qb->setMaxResults(10);

        return $qb
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Photo[] Returns an array of Photo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Photo
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
