<?php

namespace App\Utils;

use App\Entity\Filter\PhotoSearchFilter;
use Elastica\Query\BoolQuery;
use Elastica\Query\Match;
use Elastica\Query\Terms;

class ElasticQueries
{
    public static function createPhotoQuery(PhotoSearchFilter $filter): BoolQuery
    {
        $boolQuery = new BoolQuery();

        if ($filter->value !== null) {
            $fieldQuery = new Match();
            $fieldQuery->setFieldQuery('title', $filter->value);
            $boolQuery->addMust($fieldQuery);
        }

        if (count($filter->tags) > 0) {
            $tagsQuery = new Terms();
            $tagsQuery->setTerms('tags', $filter->tags);
            $boolQuery->addMust($tagsQuery);
        }

        return $boolQuery;
    }
}